import React, {useEffect, useState} from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import axios from 'axios';
import moment from 'moment';

import {Container, ContainerTurnos, RegiaoEstadoCidade, RegiaoEstadoCidadeText} from './styles';

const Graficos = ({route, navigation}) => {
  const {id} = route.params;
  const [leitura, setLeitura] = useState();
  const [municipio, setMunicipio] = useState('');
  const [estado, setEstado] = useState('');
  let rows = [1, 2, 3, 4, 5];

  useEffect(() => {
    async function getMunicipio() {
      const response = await axios.get(
        `https://apiprevmet3.inmet.gov.br/previsao/${id}`,
      );
      const data = response.data;
      //o id no caso é o numero do municipio com isso posso pegar os valores no json
      // console.log(data[id]['26/09/2020']['manha']['temp_max']);

      setLeitura(data);
    }
    getMunicipio();
  }, []);

  useEffect(() => {
    async function getMun() {
      const response = await axios.get(
        `https://servicodados.ibge.gov.br/api/v1/localidades/municipios/${id}
        `,
      );
      const data2 = response.data;
      //o id no caso é o numero do municipio com isso posso pegar os valores no json
      // console.log(data[id]['26/09/2020']['manha']['temp_max']);

      setMunicipio(data2.nome);
      setEstado(data2.microrregiao.mesorregiao.UF.nome);
    }
    getMun();
  }, []);

  return (
    <>
      {/* apenas ler quando tiver leitura, assim ele não passa antes no componente e mostra vazios */}
      {/* lembrando que o inmet so tem previsão de periodos dos dois primeiros dias os outros são resumos 
        formatação do json com os dois primeiros dias leitura[id][completo]['manha']['temp_max']
        formatação json 3 ultimos dias leitura[id][completo]['temp_max']
      */}
      <RegiaoEstadoCidade>
        <RegiaoEstadoCidadeText>Estado: {estado}</RegiaoEstadoCidadeText>
        <RegiaoEstadoCidadeText>Municipio: {municipio}</RegiaoEstadoCidadeText>
      </RegiaoEstadoCidade>
      <ScrollView>
        {leitura != null &&
          rows.map((content, index) => {
            const completo = moment().add(index, 'days').format('DD/MM/YYYY');
            return (
              <Container key={index}>
                <ContainerTurnos>
                  <Text>Data: {completo} </Text>
                  
                  <Image
                    style={index == 0 || index == 1 ?  { width: 100, height: 100} : {width: 0, height: 0} }
                    source={{
                      uri:
                        index == 0 || index == 1
                          ? leitura[id][completo]['manha']['icone'] + '"'
                          : '"' + leitura[id][completo]['icone'] + '"',
                    }}
                  />

                  <Text>Temperatura Mínima:</Text>

                  <Text>
                    {index == 0 || index == 1
                      ? leitura[id][completo]['manha']['temp_min'] + "° Graus"
                      : leitura[id][completo]['temp_min'] + "° Graus"}
                  </Text>

                  <Text>Temperatura Máxima:</Text>
                  <Text>
                    {index == 0 || index == 1
                      ? leitura[id][completo]['manha']['temp_max'] + "° Graus"
                      : leitura[id][completo]['temp_max'] + "° Graus"}
                  </Text>
                </ContainerTurnos>
              </Container>
            );

            //para que carreguem apenas 1 vez os 5 primeiros dias das previsões, então
            //setei a leitura como null
            setLeitura(null);
          })}
      </ScrollView>
    </>
  );
};

export default Graficos;
