import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import {Picker} from '@react-native-community/picker';

import {Container} from './stylesRegiao';

import api from '../../service/api';

const Regiao = ({navigation}) => {
  const [ufs, setUfs] = useState([]);
  const [uf, setUf] = useState('');
  const [municipios, setMunicipios] = useState([]);
  const [mun, setMun] = useState();

  useEffect(() => {
    async function getStates() {
      const response = await api.get();
      const data = response.data;
      setUfs(data);
      
    }
    getStates();
  }, []);

  async function getMunicipios(itemValue) {
    const response = await api.get(
      `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${itemValue}/municipios`,
    );
    const data = response.data;
    setMunicipios(data);
  }

  return (
    <>
      <Container>
        <Text>Selecione uma região</Text>
        <Picker
          selectedValue={uf}
          style={{height: 50, width: 200}}
          onValueChange={(itemValue, itemIndex) => {
            setUf(itemValue);
            getMunicipios(itemValue);
            setMun('');
          }}>
          {ufs.map((item, index) => {
            return (
              <Picker.Item label={item.nome} value={item.id} key={item.id} />
            );
          })}
        </Picker>

        {municipios._X !== null && (
          <Picker
            selectedValue={mun}
            style={{height: 50, width: 200}}
            onValueChange={(itemValue) => {
              setMun(itemValue);
              
              navigation.navigate('Graficos', {
                id: itemValue,
                mun: mun,
                
              });
            }}>
            <Picker.Item label="Selecione" value="" />
            {municipios.map((item) => {
              return (
                <Picker.Item label={item.nome} value={item.id} key={item.id} />
              );
            })}
          </Picker>
        )}
      </Container>
    </>
  );
};

export default Regiao;
