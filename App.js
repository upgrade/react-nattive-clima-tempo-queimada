import 'react-native-gesture-handler';
import React from 'react';

import Router from './src/router';

const App: () => React$Node = () => {
  return (
    <>
      <Router />
    </>
  );
};

export default App;
