import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

import {Container, CorpoBotaoNavegacao} from './styles';



const TelaInicial = ({navigation}) => {
  return (
    <>
     <Container>
        <CorpoBotaoNavegacao >
          <Button title="Dados Gerais de Focos de Queimadas" onPress={() => navigation.navigate('DadosGerais')} />
        </CorpoBotaoNavegacao>
        <Button  title="Dados do clima por Região" onPress={() => navigation.navigate('Regiao')} />
     </Container>
    </>
  );
};

export default TelaInicial;
