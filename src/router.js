import React from 'react';
import {View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

// import { Container } from './styles';

const {Navigator, Screen} = createStackNavigator();

import Regiao from './components/Regiao/Regiao';
import Graficos from './components/Graficos/Graficos';
import TelaInicial from './components/Inicial/TelaInicial'
import DadosGerais from './components/DadosGerais';

const Router = () => {
  return (
    <NavigationContainer>
      <Navigator initialRouteName="TelaInicial">
        <Screen name="TelaInicial" component={TelaInicial} options={{title: 'Escolha qual serviço deseja', headerTitleAlign: "center"}} />
        <Screen name="Regiao" component={Regiao} />
        <Screen
          name="Graficos"
          component={Graficos}
          options={{title: 'Dados Meteorologico'}}
        />
        <Screen name="DadosGerais" component={DadosGerais} options={{title: "Dados gerais focos de queimadas"}}  />
      </Navigator>
    </NavigationContainer>
  );
};

export default Router;
