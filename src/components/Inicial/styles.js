import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content:  center;
  align-items:  center;
  
`;
  
export const CorpoBotaoNavegacao = styled.View`
  margin-bottom: 40px;
  background-color: red;
  color: #000;
`;