import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const ContainerTurnos = styled.View`
  border-bottom-color: #008000;
  border-bottom-width: 2px;
  margin-bottom: 10px;
  width: 100%;
  align-items: center;
`;

export const RegiaoEstadoCidade = styled.View`
  align-items: center;
  background-color: #008000;
  margin-bottom: 10px;
  padding: 10px 0;
`;
export const RegiaoEstadoCidadeText = styled.Text`
  color: #fff;
`;
