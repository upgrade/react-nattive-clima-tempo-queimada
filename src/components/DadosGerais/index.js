import React, {useEffect, useState} from 'react';
import {View, Text, Lis} from 'react-native';
import axios from 'axios';
import {Picker} from '@react-native-community/picker';

import {Container, TituloTodos} from './styles';
import {FlatList} from 'react-native-gesture-handler';

const index = () => {
  const [paises, setPaises] = useState([]);
  const [pais, setPais] = useState('');

  const [estados, setEstados] = useState([]);
  const [estado, setEstado] = useState('');

  const [municipios, setMunicipios] = useState([]);
  const [municipio, setMunicipio] = useState('');

  //pegando todos os paises e a contagem de focus deles, sem filtro
  const [todosPaises, setTodosPaises] = useState('');

  const [totalPorSelecao, setTotalPorSelecao] = useState('');

  const cidade = 'French Guiana';

  useEffect(() => {
    async function getPaises() {
      const response = await axios.get(
        'http://queimadas.dgi.inpe.br/queimadas/dados-abertos/api/auxiliar/paises',
      );
      const data = response.data;
      setPaises(data);
      const responsecount = await axios.get(
        'http://queimadas.dgi.inpe.br/queimadas/dados-abertos/api/focos/count',
      );
      const datacount = responsecount.data;
      setTodosPaises(datacount);
    }
    getPaises();
  }, []);

  async function getEstado(itemValue) {
    const response = await axios.get(
      `http://queimadas.dgi.inpe.br/queimadas/dados-abertos/api/auxiliar/estados?pais_id=${itemValue}`,
    );
    const data = response.data;
    setEstados(data);

    const responsefoco = await axios.get(
      `http://queimadas.dgi.inpe.br/queimadas/dados-abertos/api/focos/count?pais_id=${itemValue}`,
    );
    const datafoco = responsefoco.data;

    setTotalPorSelecao(datafoco);
  }

  async function getMunicipios(itemValue) {
    const response = await axios.get(
      `http://queimadas.dgi.inpe.br/queimadas/dados-abertos/api/auxiliar/municipios?pais_id=${pais}&estado_id=${itemValue}`,
    );
    const data = response.data.sort();
    setMunicipios(data);
  }

  async function getTotalFoco(pais, estado, municipio) {
    if (pais !== null && estado === null && municipio === null) {
      setEstados([]);
      setMunicipios([]);
      const responsefoco = await axios.get(
        `http://queimadas.dgi.inpe.br/queimadas/dados-abertos/api/focos/count?pais_id=${pais}`,
      );
      const datafoco = responsefoco.data;

      setTotalPorSelecao(datafoco);
      console.log('Total: ', datafoco);
    }
    if (pais !== null && estado !== null && municipio === null) {
      setMunicipios([]);
      const responsefoco = await axios.get(
        `http://queimadas.dgi.inpe.br/queimadas/dados-abertos/api/focos/count?pais_id=${pais}&estado_id=${estado}`,
      );
      const datafoco = responsefoco.data;

      setTotalPorSelecao(datafoco);
    }
    if (pais !== null && estado !== null && municipio !== null) {
      const responsefoco = await axios.get(
        `http://queimadas.dgi.inpe.br/queimadas/dados-abertos/api/focos/count?pais_id=${pais}&estado_id=${estado}&municipio_id=${municipio}`,
      );
      const datafoco = responsefoco.data;

      setTotalPorSelecao(datafoco);
    }
  }

  return (
    <>
      <Container>
        <Picker
          selectedValue={pais}
          style={{height: 50, width: 200, marginBottom: 30}}
          onValueChange={(itemValue, itemIndex) => {
            setPais(itemValue);
            getTotalFoco(itemValue, null, null);
            getEstado(itemValue);
          }}>
          <Picker.Item label={'Selecion um País'} value={null} />
          {paises &&
            paises.map((item, index) => {
              return (
                <Picker.Item
                  label={item.pais_name}
                  value={item.pais_id}
                  key={item.pais_id}
                />
              );
            })}
        </Picker>

        <Picker
          selectedValue={estado}
          style={{height: 50, width: 200, marginBottom: 30}}
          onValueChange={(itemValue, itemIndex) => {
            setEstado(itemValue);
            getTotalFoco(pais, itemValue, null);
            itemValue ? getMunicipios(itemValue) : '';
            setMunicipio('');
          }}>
          <Picker.Item label={'Selecione um estado'} value={null} />
          {estados &&
            estados.map((item, index) => {
              return (
                <Picker.Item
                  label={item.estado_name}
                  value={item.estado_id}
                  key={item.estado_id}
                />
              );
            })}
        </Picker>

        <Picker
          selectedValue={municipio}
          style={{height: 50, width: 200, marginBottom: 30}}
          onValueChange={(itemValue, itemIndex) => {
            setMunicipio(itemValue);
            getTotalFoco(pais, estado, itemValue);
          }}>
          <Picker.Item label={'Selecione um municipio'} value={null} />
          {municipios &&
            municipios.map((item, index) => {
              return (
                <Picker.Item
                  label={item.municipio_name}
                  value={item.municipio_id}
                  key={item.municipio_id}
                />
              );
            })}
        </Picker>

        <View>
          {totalPorSelecao.Brasil && (
            <Text>Total: {totalPorSelecao.Brasil} </Text>
          )}
          {totalPorSelecao.Argentina && (
            <Text>Total: {totalPorSelecao.Argentina} </Text>
          )}

          {totalPorSelecao.Colombia && (
            <Text>Total: {totalPorSelecao.Colombia} </Text>
          )}

          {totalPorSelecao.Paraguay && (
            <Text>Total: {totalPorSelecao.Paraguay} </Text>
          )}

          {totalPorSelecao.Peru && <Text>Total: {totalPorSelecao.Peru} </Text>}

          {totalPorSelecao.Venezuela && (
            <Text>Total: {totalPorSelecao.Venezuela} </Text>
          )}

          {totalPorSelecao.Guyana && (
            <Text>Total: {totalPorSelecao.Guyana} </Text>
          )}

          {totalPorSelecao.Suriname && (
            <Text>Total: {totalPorSelecao.Suriname} </Text>
          )}

          {totalPorSelecao.Chile && (
            <Text>Total: {totalPorSelecao.Chile} </Text>
          )}


          {totalPorSelecao.Uruguay && (
            <Text>Total: {totalPorSelecao.Uruguay} </Text>
          )}

          {totalPorSelecao.Ecuador && (
            <Text>Total: {totalPorSelecao.Ecuador} </Text>
          )}

          {totalPorSelecao.Bolivia && (
            <Text>Total: {totalPorSelecao.Bolivia} </Text>
          )}
        </View>
      </Container>

      <Container>
        <TituloTodos>
          Contagem de Focos de Queimadas de Todos os Países
        </TituloTodos>
        <View style={{marginTop: 20}}>
          <Text> Argentina: {todosPaises.Argentina} </Text>
          <Text> Bolívia: {todosPaises.Bolivia} </Text>
          <Text> Brasil: {todosPaises.Brasil} </Text>
          <Text> Colombia: {todosPaises.Colombia} </Text>
          <Text> Paraguai: {todosPaises.Paraguay} </Text>
          <Text> Peru: {todosPaises.Peru} </Text>
          <Text> Venezuela: {todosPaises.Venezuela} </Text>
        </View>
      </Container>
    </>
  );
};

export default index;
